/*
 * TCP compression support routines
 *
 */

#include <linux/module.h>
#include <linux/types.h>
#include <linux/tcp.h>
#include <linux/zlib.h>
#include <linux/vmalloc.h>

#include <net/tcp.h>
#include <net/ip.h>

#include <asm/uaccess.h>

int sysctl_tcp_comp_version __read_mostly = TCP_COMP_VERSION_DEFAULT;

/*
 * replaces all the skbs starting from @skb with the single skb @nskb
 * in the write queue of socket @sk
 */
static void replace_skb_list(struct sk_buff *nskb, struct sk_buff *skb,
			       struct sock *sk, struct tcp_sock *tp)
{
	struct sk_buff *next;

	/* remove the old skbuffs from the queue */
	tcp_insert_write_queue_before(nskb, skb, sk);
	while (skb != (struct sk_buff*)&(sk)->sk_write_queue) {
		next = tcp_write_queue_next(sk, skb);
		tcp_unlink_write_queue(skb, sk);
		sk_stream_free_skb(sk, skb);
		skb = next;
	}
	tp->compression_head = NULL;
}

static void update_comp_tcpcb_info(struct sk_buff *nskb, struct sk_buff *skb,
			  	   struct sock *sk, struct tcp_sock *tp,
				   u32 uncompressed_len)
{
	TCP_SKB_CB(nskb)->sacked = 0;
	TCP_SKB_CB(nskb)->seq = TCP_SKB_CB(skb)->seq;
/*	TCP_SKB_CB(nskb)->end_seq = TCP_SKB_CB(skb)->seq + nskb->len;*/
	TCP_SKB_CB(nskb)->end_seq = TCP_SKB_CB(skb)->seq + uncompressed_len;
	TCP_SKB_CB(nskb)->compression_flags |= TCPCB_COMPRESSED;
	TCP_SKB_CB(nskb)->compressed_len = nskb->len;
	TCP_SKB_CB(nskb)->uncompressed_len = uncompressed_len;
	TCP_SKB_CB(nskb)->when = tcp_time_stamp;
	/*printk(KERN_DEBUG "TCPCB: UL=%d L=%d SEQ=%d\n",
		TCP_SKB_CB(nskb)->uncompressed_len,
		TCP_SKB_CB(nskb)->compressed_len,
		TCP_SKB_CB(nskb)->seq);*/
}

struct zlib_compression_state
{
	z_stream zstr;
};

int zlib_compression_callback(const struct sk_buff *skb, void *data, int offset,
				int len, void *state)
{
	struct zlib_compression_state *comp_state;
	int res;

	comp_state = (struct zlib_compression_state*)state;
	comp_state->zstr.next_in = data;
	comp_state->zstr.avail_in = len;
	res = zlib_deflate(&comp_state->zstr, 0);
	if (&comp_state->zstr.avail_out == 0)
		return -E2BIG;
	if (res != Z_OK)
		return -EFAULT;
	return len;
}

int tcp_compress_write_queue_zlib(struct sock *sk, struct tcp_sock *tp)
{
	struct zlib_compression_state comp_state;
	unsigned int tot_len;
	struct sk_buff *skb, *nskb;
	int res = -EINVAL;

	skb = tp->compression_head;	
	tot_len = 0;
	tcp_for_write_queue_from(skb, sk)
		tot_len += skb->len;
	if (!tot_len)
		return 0;

	/*printk(KERN_DEBUG "Sock:%p Tp:%p tot_len:%d\n", sk, tp, tot_len);*/
	memset(&comp_state, 0, sizeof(struct zlib_compression_state));
	comp_state.zstr.workspace = kmalloc(zlib_deflate_workspacesize(), GFP_ATOMIC);
	if(!comp_state.zstr.workspace) {
		res = -ENOMEM;
		goto err_alloc_workspace;
	}
	if (zlib_deflateInit(&comp_state.zstr, Z_DEFAULT_COMPRESSION))
		goto err_init_deflate;

	nskb = sk_stream_alloc_skb(sk, tot_len, GFP_ATOMIC);
	if(nskb == NULL) {
		res = -ENOMEM;
		goto err_alloc_skb;
	}
	sk_charge_skb(sk, nskb);

	comp_state.zstr.next_out = skb_put(nskb, tot_len);
	comp_state.zstr.avail_out = tot_len;	

	skb = tp->compression_head;
	nskb->csum = 0;
	nskb->ip_summed = skb->ip_summed;
	tcp_for_write_queue_from(skb, sk) {
		TCP_SKB_CB(nskb)->flags |= TCP_SKB_CB(skb)->flags;
		res = skb_walk_data(skb, 0, skb->len, 
				    zlib_compression_callback, 
				    &comp_state); 
		if (res < 0)
			goto err_compression;
	}
	res = zlib_deflate(&comp_state.zstr, Z_FINISH);
	if (res != Z_STREAM_END) {
		if (comp_state.zstr.avail_out == 0)
			res = -E2BIG;
		else
			res = -EFAULT;
		goto err_compression;
	}

	BUG_ON(comp_state.zstr.total_in != tot_len);
	skb_trim(nskb, comp_state.zstr.total_out);
	/*printk(KERN_DEBUG "nskb:%p UL:%d L:%d\n", nskb, tot_len, nskb->len);*/
	if (nskb->ip_summed == CHECKSUM_NONE)
		nskb->csum = skb_checksum(nskb, 0, nskb->len, nskb->csum);

	skb = tp->compression_head;
	update_comp_tcpcb_info(nskb, skb, sk, tp, tot_len);
	replace_skb_list(nskb, skb, sk, tp);

	zlib_deflateEnd(&comp_state.zstr);
	kfree(comp_state.zstr.workspace);
	return 0;

err_compression:
	sk_stream_free_skb(sk, nskb);
err_alloc_skb:
	zlib_deflateEnd(&comp_state.zstr);
err_init_deflate:
	kfree(comp_state.zstr.workspace);
err_alloc_workspace:
	/*printk(KERN_DEBUG "zlib_err: %d\n", res);*/
	return res;
}

int zlib_decompression_callback(const struct sk_buff *skb, void *data, int offset,
	 			int len, void *state)
{
	struct zlib_compression_state *comp_state;
	int res;

	comp_state = (struct zlib_compression_state*)state;
	comp_state->zstr.next_in = data;
	comp_state->zstr.avail_in = len;
	res = zlib_inflate(&comp_state->zstr, 0);
	if (res != Z_OK && res != Z_STREAM_END)
		return -EFAULT;
	return len;
}

/*
 * Purge the decompression list of socket tp, removing
 * all the skbs from the range of the first compressed skb
 */
static void purge_decomp_queue(struct tcp_sock *tp) 
{
	struct sk_buff *head_skb;
	unsigned int comp_len, end_seq;
	head_skb = skb_peek(&tp->decompression_queue);
	comp_len = TCP_SKB_CB(head_skb)->compressed_len;
	end_seq = TCP_SKB_CB(head_skb)->seq + comp_len;
	for (;;) {
		head_skb = skb_peek(&tp->decompression_queue);
		if (head_skb == NULL ||
		    !before(TCP_SKB_CB(head_skb)->seq, end_seq))
			break;

		if (comp_len < head_skb->len) {
			__skb_pull(head_skb, comp_len);
			TCP_SKB_CB(head_skb)->seq += comp_len;
			break;
		}
		comp_len -= head_skb->len;
		__skb_unlink(head_skb, &tp->decompression_queue);
		__kfree_skb(head_skb);
	}

}

int tcp_decompress_queue_zlib(struct sock *sk, struct tcp_sock *tp)
{
	struct zlib_compression_state comp_state;
	struct sk_buff *head_skb, *nskb;
	unsigned int comp_len, uncomp_len, hdr_len, end_seq, data_len;
	int res = -EINVAL;

	head_skb = skb_peek(&tp->decompression_queue);
	comp_len = TCP_SKB_CB(head_skb)->compressed_len;
	uncomp_len = TCP_SKB_CB(head_skb)->uncompressed_len;
	end_seq = TCP_SKB_CB(head_skb)->seq + comp_len;

	memset(&comp_state, 0, sizeof(struct zlib_compression_state));
	comp_state.zstr.workspace = kmalloc(zlib_inflate_workspacesize(), GFP_ATOMIC);
	if(!comp_state.zstr.workspace) {
		res = -ENOMEM;
		goto err_alloc_workspace;
	}
	if (zlib_inflateInit(&comp_state.zstr))
		goto err_init_deflate;

	hdr_len = skb_headroom(head_skb);
	nskb = alloc_skb(hdr_len + uncomp_len, GFP_ATOMIC);
	if(nskb == NULL) {
		res = -ENOMEM;
		goto err_alloc_skb;
	}

	/* Copy the protocol headers and control block */
	skb_set_mac_header(nskb, (skb_mac_header(head_skb) - 
				  head_skb->head));
	skb_set_network_header(nskb, (skb_network_header(head_skb) - 
				      head_skb->head));
	skb_set_transport_header(nskb, (skb_transport_header(head_skb) - 
					head_skb->head));
	skb_reserve(nskb, hdr_len);
	memcpy(nskb->head, head_skb->head, hdr_len);
	memcpy(nskb->cb, head_skb->cb, sizeof(head_skb->cb));
	comp_state.zstr.next_out = skb_put(nskb, uncomp_len);
	comp_state.zstr.avail_out = uncomp_len;	

	/* Update the CB for the new skb */
	TCP_SKB_CB(nskb)->compression_flags &= ~TCPCB_COMPRESSED;
	TCP_SKB_CB(nskb)->compressed_len = 0;
	TCP_SKB_CB(nskb)->uncompressed_len = 0;
	TCP_SKB_CB(nskb)->end_seq = TCP_SKB_CB(head_skb)->seq + uncomp_len;

	/* Decompress the list of skbs */
#if 0
	printk(KERN_DEBUG "Decompressing %u to %u\n", comp_len, uncomp_len);
#endif
	head_skb = skb_peek(&tp->decompression_queue);
	for (;;) {
		if (!before(TCP_SKB_CB(head_skb)->seq, end_seq))
			break;

		data_len = comp_len;
		if (head_skb->len < data_len)
			data_len = head_skb->len;
		res = skb_walk_data(head_skb, 0, data_len, 
				    zlib_decompression_callback, 
				    &comp_state); 
		if (res < 0)
			goto err_decompression;
		BUG_ON(res != data_len);
		comp_len -= data_len;

		if (head_skb == skb_peek_tail(&tp->decompression_queue))
			break;
		head_skb = head_skb->next;
	}

	res = zlib_inflate(&comp_state.zstr, Z_FINISH);
	if (res != Z_STREAM_END) {
		if (comp_state.zstr.avail_out == 0)
			res = -E2BIG;
		else
			res = -EFAULT;
		goto err_decompression;
	}
	zlib_inflateEnd(&comp_state.zstr);

#if 0
	printk(KERN_DEBUG "Queuing packet of %d\n", nskb->len);
#endif
	purge_decomp_queue(tp);
	__skb_queue_tail(&sk->sk_receive_queue, nskb);
	sk_stream_set_owner_r(nskb, sk);
	if (tp->rcv_nxt == end_seq)
		tp->rcv_nxt = TCP_SKB_CB(nskb)->end_seq;
	return 0;

err_decompression:
	__kfree_skb(nskb);
err_alloc_skb:
	zlib_inflateEnd(&comp_state.zstr);
err_init_deflate:
	kfree(comp_state.zstr.workspace);
err_alloc_workspace:
	purge_decomp_queue(tp);	
	return res;
}

typedef int (*tcp_compress_queue_fn)(struct sock *sk, struct tcp_sock *tp);

tcp_compress_queue_fn compress_fn_list[] = 
{
	NULL,
	tcp_compress_write_queue_zlib,
	NULL,
	NULL
};

tcp_compress_queue_fn decompress_fn_list[] =
{
	NULL,
	tcp_decompress_queue_zlib,
	NULL,
	NULL
};

int tcp_compress_write_queue(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);
	if ((1 << sk->sk_state) & ~(TCPF_ESTABLISHED | TCPF_CLOSE_WAIT))
		return 0; /* no compression after FIN or before SYN/ACK */
	if (tp->rx_opt.comp_ver >= ARRAY_SIZE(compress_fn_list))
		return -EINVAL; 
	if (!tp->rx_opt.comp_ver || !tp->compression_head)
		return 0;	
	return compress_fn_list[tp->rx_opt.comp_ver](sk, tp);
}

void tcp_queue_received_skb(struct sock *sk, struct sk_buff *skb)
{
	struct tcp_sock *tp = tcp_sk(sk);
	if ((TCP_SKB_CB(skb)->compression_flags & TCPCB_COMPRESSED) == 0 &&
	    skb_queue_empty(&tp->decompression_queue))
		__skb_queue_tail(&sk->sk_receive_queue, skb);
	else
		__skb_queue_tail(&tp->decompression_queue, skb);
}

int tcp_decompress_queue(struct sock *sk)
{
	struct tcp_sock *tp = tcp_sk(sk);
	struct sk_buff *head_skb, *tail_skb;
	u32 compressed_range_end;
	int res;

	if (tp->rx_opt.comp_ver >= ARRAY_SIZE(compress_fn_list))
		return -EINVAL; 
	if (!tp->rx_opt.comp_ver) {
		/* Compression is disabled */
		BUG_ON(!skb_queue_empty(&tp->decompression_queue));
		return 0;	
	}

	for (;;) {
		head_skb = skb_peek(&tp->decompression_queue);
		if (head_skb == NULL)
			break;

		/* If the head is not compressed, 
		 * move it to the receive queue
		 */
		if ((TCP_SKB_CB(head_skb)->compression_flags & TCPCB_COMPRESSED) == 0) {
			__skb_unlink(head_skb, &tp->decompression_queue);
			__skb_queue_tail(&sk->sk_receive_queue, head_skb);
			continue;
		}

		/* Check if we have enough data to decompress */
		tail_skb = skb_peek_tail(&tp->decompression_queue);
		compressed_range_end = TCP_SKB_CB(head_skb)->seq + 
			TCP_SKB_CB(head_skb)->compressed_len; 
		if (before(TCP_SKB_CB(tail_skb)->end_seq, compressed_range_end))
			break;

		res = decompress_fn_list[tp->rx_opt.comp_ver](sk, tp);
		if (res != 0)
			return res;
	}
	return 0;
}

